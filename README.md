# cypress-visual-regression-test

## Development

Get Docker https://www.docker.com/get-started

### Local development with local DNS nameserver (For iOS & Android)

The `./start.sh` script runs `docker-compose -f docker-compose.yaml -f docker-compose.test.yaml up -d` to spin up the web front end, Nginx and dnsmasq services. Then it sets the locally running dnsmasq container as the DNS server for resolving the development service domains. The Nginx runs as a reverse proxy for the mentioned upstream services.

To start the services locally:

```$ ./start.sh```

To stop the services:

```$ ./stop.sh```

### Checking logs

```$ docker-compose logs -f```
