#!/bin/bash

printf "\nWaiting for http://webhack.local.com"

# Wait 10 minutes for the 200 HTTP reponse from http://webhack.local.com
attemp=1
max_attemps=200
until $(curl --output /dev/null --silent --head --fail --insecure http://webhack.local.com); do
    if [ ${attemp} -gt ${max_attemps} ]; then
        printf "\nError: webhack is not available after 10 minutes"
        exit 1
    fi
    printf '.'
    attemp=$(($attemp+1))
    sleep 3
done

#Define cleanup procedure
cleanup() {
    chmod -R o+w /app/cypress/screenshots
    chmod -R o+w /app/cypress/snapshots
}

#Trap SIGTERM
trap 'true' SIGTERM

#Execute command
printf "\n> /app/node_modules/.bin/cypress $*"
/app/node_modules/.bin/cypress $* &
cypress_pid=$!

#Wait
wait $cypress_pid
exit_code=$?

#Cleanup
cleanup

exit $exit_code
