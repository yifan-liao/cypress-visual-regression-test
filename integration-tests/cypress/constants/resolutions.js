export const RESOLUTIONS = [
  {
    width: 1920,
    height: 1440,
  },
  {
    width: 1440,
    height: 1440,
  },
  {
    width: 1024,
    height: 768,
  },
];

export const MOBILE_RESOLUTIONS = [
  {
    width: 768,
    height: 1024,
  },
  {
    width: 540,
    height: 768,
  },
  {
    width: 375,
    height: 667,
  },
];
