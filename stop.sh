echo 'Removing nameserver 127.0.0.1 from DNS setting ...'
device="$(route get google.com | grep interface | awk '{print $2}')"
interface="$(networksetup -listnetworkserviceorder | grep $device | awk 'BEGIN {FS=", "} {print $1}' | awk 'BEGIN {FS=": "} {print $2}')"

# clean up 127.0.0.1 in /etc/resolv.conf
local_nameserver=$(grep '127.0.0.1' /etc/resolv.conf)
if  [ -n "$local_nameserver" ]
then
  nameservers=$(grep 'nameserver' /etc/resolv.conf | grep -v '127.0.0.1' | awk '{print $2}')
  if [ -z "$nameservers" ]; then nameservers=empty; fi
  echo "networksetup -setdnsservers ${interface} ${nameservers}"
  networksetup -setdnsservers $interface $nameservers
fi

docker-compose -f docker-compose.yaml down
