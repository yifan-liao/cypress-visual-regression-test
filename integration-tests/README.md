# Integration tests

The Cypress integration tests.

## Test setup

```
$ npm run test:build
```

To run integration tests:
```
$ npm run test
```
To run a specific integration test:
```
$ npm run test -- --spec cypress/integration/sample.spec.js
```
To run integration tests and update all snapshots:
```
$ npm run test:update-snapshot
```

To run integration tests and update snapshots for a specific test:
```
$ npm run test:update-snapshot -- --spec cypress/integration/sample.spec.js
```

There's a bug in Cypress that currently we can't ignore files.

https://github.com/cypress-io/cypress/issues/1696

## Checking test failures

The screenshots for test failures can be found in `cypress/screenshots`.

The diff of visual regression tests can be found in `__diff_output__` in `cypress/snapshots`.
