// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const {
  addMatchImageSnapshotPlugin,
} = require('cypress-image-snapshot/plugin');

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('before:browser:launch', (browser = {}, args) => {
    // browser will look something like this
    // {
    //   name: 'chrome',
    //   displayName: 'Chrome',
    //   version: '63.0.3239.108',
    //   path: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    //   majorVersion: '63'
    // }

    if (browser.name === 'chrome') {
      // for fullHD page screenshot we need a bigger window size to make sure the screenshots are not shrinked
      args.push('--window-size=2560,1440');
      // when running in fullscreen is required
      // args.push('--start-fullscreen')

      // https://github.com/cypress-io/cypress/issues/2102
      // the issue suggests the following args can be used but doesn't work in Docker container
      // args.push('--cast-initial-screen-width=2560');
      // args.push('--cast-initial-screen-height=1440');

      // for checking Chrome launch args
      // console.log('  Launching Chrome...\n', args);

      // whatever you return here becomes the new args
      return args;
    }

    if (browser.name === 'electron') {
      args.width = 2560;
      args.height = 1440;
      // when running in fullscreen is required
      // args['fullscreen'] = true

      // for checking Electron launch args
      // console.log('  Launching Electron...\n', args);
      return args;
    }
  });

  // console.log('Adding cypress-image-snapshot plugin...\n', config);
  addMatchImageSnapshotPlugin(on, config);
};
