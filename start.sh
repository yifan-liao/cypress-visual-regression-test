docker-compose build
docker-compose -f docker-compose.yaml up -d

echo 'Adding nameserver 127.0.0.1 to DNS setting ...'

device="$(route get google.com | grep interface | awk '{print $2}')"
interface="$(networksetup -listnetworkserviceorder | grep $device | awk 'BEGIN {FS=", "} {print $1}' | awk 'BEGIN {FS=": "} {print $2}')"

# clean up 127.0.0.1 in /etc/resolv.conf
local_nameserver=$(grep '127.0.0.1' /etc/resolv.conf)
if  [ -n "$local_nameserver" ]
then
  default_nameservers=$(grep 'nameserver' /etc/resolv.conf | grep -v '127.0.0.1' | awk '{print $2}')
  if [ -z "$default_nameservers" ]; then default_nameservers=empty; fi
  networksetup -setdnsservers $interface $default_nameservers
fi

nameservers=$(sed -ne 's/^ *nameserver  *\([.:0-9A-Fa-f][.:0-9A-Fa-f]*\)/\1/p' /etc/resolv.conf)
if [ -z "$nameservers" ]; then nameservers=empty; fi

echo "networksetup -setdnsservers ${interface} 127.0.0.1 ${nameservers}"
networksetup -setdnsservers $interface 127.0.0.1 $nameservers

echo 'Open http://webhack.local.com'

open http://webhack.local.com
